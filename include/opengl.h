/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opengl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:54:14 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:03:20 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPENGL_H
# define OPENGL_H

# ifdef __APPLE__
# 	define GLFW_INCLUDE_GLCOREARB
# endif
# include <ctype.h>
# if defined(_WIN32) || defined(__CYGWIN__)
# 	include <windef.h>
#	define GLEW_STATIC
# 	include "GL/glew.h"
# endif
# include "GLFW/glfw3.h"
# include "list.h"

typedef struct	s_texture
{
	GLuint			id;
	unsigned int	width;
	unsigned int	height;
	unsigned int	image_size;
	unsigned char	*data;
}				t_texture;

typedef struct	s_obj
{
	t_list		*vertices;
	t_list      *indices;

	t_list      **vert_faces;
	float       *faces_norm;
	float       *vert_norm;
	float  		*colors;
	float		*uv;
	
	//t_list		*lines_list;
	GLuint		vao;
	GLuint		points_vbo;
	GLuint		colors_vbo;
	GLuint		uv_vbo;
	GLuint		normals_vbo;
	GLuint      indices_ebo;
	//GLuint		lines_ebo;

	float		center[3];
	float       size;
	t_texture	texture;
}				t_obj;

typedef struct  s_sc_obj
{
	float      *vertices;
	float      *tex_coord;

	GLuint      vao;
	GLuint      vertices_vbo;
	GLuint      tex_vbo;

	GLuint      frame_buf;
	GLuint      frame_tex;
	GLuint      rbo_depth;

	GLuint      msaa_buf;
	GLuint      msaa_tex;

}               t_sc_obj;

t_obj			*parse_file(const char *filename);

t_obj			*load_object(const char *filename, const GLuint program_id);
t_sc_obj		*load_sc_object(const GLuint program_id);

GLuint			load_2d_vbo(const GLuint program_id, GLsizeiptr size,
	const GLvoid *data, const char *name);
GLuint			load_3d_vbo(const GLuint program_id, GLsizeiptr size,
	const GLvoid *data, const char *name);
GLuint			load_ebo(GLsizeiptr size, const GLvoid *data);
GLuint			load_col_vbo(const GLuint program_id, GLsizeiptr size,
	const GLvoid *data, const char *name);

GLuint			load_bmp_texture(const char *imagepath, t_texture *texture);
int				load_uv(t_obj *obj, const GLuint program_id);
float			*create_uv_datas(float *uv, t_list *vertices, int dir);

unsigned int	check_error(void);

#endif
