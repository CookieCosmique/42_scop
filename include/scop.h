/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:50:29 by bperreon          #+#    #+#             */
/*   Updated: 2016/01/12 14:41:35 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>

# include "ft_log.h"
# include "opengl.h"
# include "shaders.h"
# include "matrix.h"

# define WIDTH 1280
# define HEIGHT 720
# define XX_ALIAS 1.0
# define MSAA 4

# define FULLSCREEN 0

# define FPS 120

enum				e_flag
{
	F_SCREEN = 1,
	F_SHADOW = 2,
	F_LINES = 4
};

typedef struct		s_matrix
{
	GLuint		id;
	float		*m;
}					t_matrix;

typedef struct		s_shader_program
{
	GLuint		id;
	t_matrix	trans;
	t_matrix	light;
	t_matrix	proj;
	t_matrix	view;
	GLuint		vertex_shader;
	GLuint		fragment_shader;
	GLuint		geometry_shader;
	GLuint		ratio_id;
	GLuint		typ_id;
	GLuint		alpha_line_id;
	GLuint		shadow_id;
	GLuint		pow_id;
	GLuint		diff_id;
	GLuint		face_id;
	GLuint		inverse_norm_id;
	GLuint		nbr_vertex_id;
	GLuint		coef_id;
}					t_shader_program;

typedef struct		s_env
{
	GLFWwindow			*window;
	t_obj				*obj;
	t_sc_obj			*sc_obj;
	t_shader_program	scene_program;
	t_shader_program	screen_program;
	t_shader_program	line_program;
	int					flags;
}					t_env;

int					init_env(t_env *env, const char *filename);
void				loop(t_env *e);
void				render(t_env *e);

//float				*create_grey_colors(const unsigned int size, const int coef);
float				*create_grey_colors(const unsigned int size);

t_matrix			load_matrix(GLuint id, const char *name, float *m);

t_shader_program	load_geo_shader_program(char *vertex_src,
	char *fragment_src, char *geometry_src);
t_shader_program	load_shader_program(char *vertex_src,
	char *fragment_src);
t_shader_program	load_shader_scene_program(void);
t_shader_program	load_shader_screen_program(void);
t_shader_program	load_shader_line_program(void);

void				*ft_memalloc(size_t s);

#endif
