/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 18:24:13 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/20 19:03:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LOG_H
# define FT_LOG_H

# include <stdio.h>
# include <stdarg.h>

typedef enum	e_logtype
{
	LOG_ERROR = 0,
	LOG_FINE = 1,
	LOG_INFO = 2,
	LOG_WARNING = 3,
	LOG_DEBUG = 4,
}				t_logtype;

int				ft_log(t_logtype type, const char *str, ...);

#endif
