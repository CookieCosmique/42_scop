/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 15:50:42 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/13 12:41:48 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

# include <stdlib.h>
# include <string.h>
# include <stdio.h>

typedef struct		s_list
{
	void	*data;
	size_t	content_size;
	size_t	size;
	size_t	allocated;
}					t_list;

t_list				*list_new(size_t const content_size);
void				list_add(t_list *list, void const *data);
void				list_remove(t_list *list, size_t index);
void				list_clear(t_list *list);
void				list_delete(t_list *list);

#endif
