/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shaders.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:55:32 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/20 19:02:59 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHADERS_H
# define SHADERS_H

# include "opengl.h"

char		*read_file(const char *filename);
GLuint		load_shader(const char *filename, GLenum shader_type);
void		shader_delete(GLuint program, GLuint shader);

#endif
