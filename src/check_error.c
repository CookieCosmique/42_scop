/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 18:08:40 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:39:10 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "opengl.h"
#include "ft_log.h"

static const char	*get_error_str(GLuint error)
{
	int					i;
	static const GLuint	error_list[5] = {GL_NO_ERROR, GL_INVALID_ENUM,
		GL_INVALID_VALUE, GL_INVALID_OPERATION, GL_OUT_OF_MEMORY};
	static const char	*error_str[5] = {"GL_NO_ERROR", "GL_INVALID_ENUM",
		"GL_INVALID_VALUE", "GL_INVALID_OPERATION", "GL_OUT_OF_MEMORY"};

	i = 0;
	while (i < 5)
	{
		if (error_list[i] == error)
			return (error_str[i]);
		i++;
	}
	return (NULL);
}

unsigned int		check_error(void)
{
	GLuint		error;

	if ((error = glGetError()))
		ft_log(LOG_ERROR, get_error_str(error));
	return (error);
}
