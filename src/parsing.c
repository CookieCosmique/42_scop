/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 15:01:17 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:30:31 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static char		ft_isspace(char c)
{
	return (c == '\t' || c == '\n' || c == ' ' || c == '\f' || c == '\v' || c == '\r');
}

static char		ft_isnumber(char c)
{
	return ((c >= '0' && c <= '9') || c == '+' || c == '-' || c == '.');
}

/*static t_list    *lineate(size_t size)
{
	t_list		*list;
	size_t		i;
	GLuint		value;

	if (!(list = list_new(sizeof(GLuint))))
		return (NULL);
	i = 0;
	while (i < size)
	{
		value = i;
		list_add(list, &value);
		value = i+1;
		list_add(list, &value);
		value = i+1;
		list_add(list, &value);
		value = i+2;
		list_add(list, &value);
		value = i+2;
		list_add(list, &value);
		value = i;
		list_add(list, &value);
		i+=3;

	}
	return (list);
}*/

static int		parse_vertice(t_list *vertices, const char *line)
{
	int		i;
	int		c;
	float	value;

	i = 0;
	c = 0;
	while (line[i])
	{
		while (line[i] && (line[i] < '0' || line[i] > '9') && line[i] != '+' &&
				line[i] != '-' && line[i] != '.')
			i++;
		if (!line[i])
			break ;
		value = atof(line + i);
		list_add(vertices, &value);
		while (line[i] && !ft_isspace(line[i]))
			i++;
		c++;
	}
	return (c == 3);
}

/*static void		parse_indices(t_list *indices, const char *line)
{
	int			i;
	int			c;
	GLuint		values[3];

	i = 0;
	c = 0;
	printf("new line : ");
	while (line[i])
	{
		while (line[i] && !ft_isnumber(line[i]))
			i++;
		if (!line[i])
			break ;
		values[0] = atoi(line + i) - 1;
		printf(": %d :",values[0]);
		{
			list_add(indices, values + 1);
		values[1] = c == 0 ? values[0] : values[1];
		if (c >= 3)
			list_add(indices, values + 2);
		}
		list_add(indices, values);
		while (line[i] && !ft_isspace(line[i]))
			i++;
		c++;
		values[2] = values[0];
	}
	printf("\n");
}*/

static void		parse_indices(t_list *indices, const char *line)
{
	int			i;
	int			c;
	GLuint		values[3];

	i = 0;
	c = 0;
	while (line[i])
	{
		while (line[i] && !ft_isnumber(line[i]))
			i++;
		if (!line[i])
			break ;
		if( c < 3)
		{
			values[c] = atoi(line + i) - 1;
			//printf(": %d :",values[c]);
		}
		else
		{
			values[1] = values[2];
			values[2] = atoi(line + i) - 1;
			//printf(": %d :",values[2]);
		}
		if(c >= 2)
		{
			//printf("new face : %d,%d,%d \n",values[0],values[1],values[2]);
			list_add(indices, values + 0);
			list_add(indices, values + 1);
			list_add(indices, values + 2);

		}
		while (line[i] && !ft_isspace(line[i]))
			i++;
		c++;
	}
}

static t_list	*simple_add(t_list *to_add, int value)
{
	if(!to_add)
		to_add = list_new(sizeof(GLuint));
	list_add(to_add, &value);
	return (to_add);
}

static t_list	**calc_vert_faces(t_list *indices, size_t size)
{
	size_t		i;
	int			j;
	t_list		**vert_faces;

	vert_faces = (t_list**)ft_memalloc(sizeof(t_list*) * (size / 3));
	i = 0;
	while(i < size / 3)
	{
		vert_faces[i] = NULL;
		i++;
	}
	i = 0;
	while(i < indices->size / 3)
	{
		j = 0;
		while(j < 3)
		{
			vert_faces[((GLuint *)indices->data)[i*3 + j]] = simple_add(vert_faces[((GLuint *)indices->data)[i*3 + j]],i);
			j++;
		}
		i++;
	}
	return (vert_faces);
}

static float	*calc_faces_norm(t_list *vertices,t_list *indices)
{
	size_t		i;
	float		*normal;
	t_point3	vert_1;
	t_point3	vert_2;
	t_point3	vert_3;
	t_point3	plan_x;
	t_point3	plan_y;
	t_point3	cross_res;

	normal = (float*)ft_memalloc(sizeof(float) * indices->size);
	i = 0;
	while(i < indices->size / 3)
	{
		vert_1 = point3(
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3] * 3],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3] * 3 + 1],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3] * 3 + 2]);
		vert_2 = point3(
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 1] * 3],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 1] * 3 + 1],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 1] * 3 + 2]);
		vert_3 = point3(
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 2] * 3],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 2] * 3 + 1],
			((float*)vertices->data)[((GLuint*)indices->data)[i * 3 + 2] * 3 + 2]);
		plan_x = vec_from(vert_1,vert_2);
		plan_y = vec_from(vert_1,vert_3);
		cross_res = cross_product(plan_x,plan_y);
		normal[i * 3] = cross_res.x;
		normal[i * 3 + 1] = cross_res.y;
		normal[i * 3 + 2] = cross_res.z;
		i++;
	}
	return (normal);
}

static float	*calc_vert_norm(float *normal, t_list **vert_faces, size_t size)
{
	float		*vert_norm;
	size_t		i;
	size_t		j;

	vert_norm = (float*)ft_memalloc(sizeof(float) * size);
	i = 0;
	while(i < size/3)
	{
		vert_norm[i * 3] = 0;
		vert_norm[i * 3 + 1] = 0;
		vert_norm[i * 3 + 2] = 0;
		if(vert_faces[i] != NULL)
		{
			j = 0;
			while(j < vert_faces[i]->size)
			{
				vert_norm[i * 3] += 
					normal[((int*)vert_faces[i]->data)[j] * 3];
				vert_norm[i * 3 + 1] += 
					normal[((int*)vert_faces[i]->data)[j] * 3 + 1];
				vert_norm[i * 3 + 2] += 
					normal[((int*)vert_faces[i]->data)[j] * 3 + 2];
				j++;
			}
			vert_norm[i*3] /= (float)j;
			vert_norm[i*3 + 1] /= (float)j;
			vert_norm[i*3 + 2] /= (float)j;
		}
		i++;
	}
	return (vert_norm);
}

/*static t_list	*combine(t_list *vertices, t_list *indices)
{
	t_list			*list;
	unsigned int	i;
	float			value;

	if (!(list = list_new(sizeof(float))))
		return (NULL);
	i = 0;
	while (i < indices->size)
	{
		value = ((float*)vertices->data)[((GLuint*)(indices->data))[i] * 3];
		list_add(list, &value);
		value = ((float*)vertices->data)[((GLuint*)(indices->data))[i] * 3 + 1];
		list_add(list, &value);
		value = ((float*)vertices->data)[((GLuint*)(indices->data))[i] * 3 + 2];
		list_add(list, &value);
		i++;
	}
	return (list);
}*/

static int		fill(const char *filename, t_list *vertices, t_list *indices)
{
	char		*line;
	ssize_t		ret;
	size_t		linecap;
	FILE		*file;

	line = (char*)ft_memalloc(1);
	if (!(file = fopen(filename, "r")) || !line)
		return (0);
	while ((ret = getline(&line, &linecap, file)) > 0)
	{
		if (line[0] == 'v')
			parse_vertice(vertices, line);
		else if (line[0] == 'f')
			parse_indices(indices, line);
	}
	return (1);
}

t_obj			*parse_file(const char *filename)
{
	t_obj		*obj;
	t_list		*vertices;
	t_list		*indices;

	if (!(obj = (t_obj*)ft_memalloc(sizeof(t_obj))))
		return (NULL);
	if (!(vertices = list_new(sizeof(float))))
		return (NULL);
	if (!(indices = list_new(sizeof(GLuint))))
		return (NULL);
	if (!fill(filename, vertices, indices))
		return (NULL);
	/*if (!(obj->vertices = combine(vertices, indices)))
		return (NULL);*/
	/*if (!(obj->lines_list = lineate(obj->vertices->size)))
		return (NULL);*/
	obj->vertices = vertices;
	obj->indices = indices;
	obj->vert_faces = calc_vert_faces(obj->indices,obj->vertices->size);
	obj->faces_norm = calc_faces_norm(obj->vertices,obj->indices);
	obj->vert_norm = calc_vert_norm(obj->faces_norm,obj->vert_faces,obj->vertices->size);
	//list_delete(vertices);
	//list_delete(indices);
	return (obj);
}
