/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loaders.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 12:45:44 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:17:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

GLuint			load_2d_vbo(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, const char *name)
{
	GLuint	vbo;
	GLuint	index;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	index = glGetAttribLocation(program_id, name);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, 0);
	return (vbo);
}

GLuint			load_3d_vbo(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, const char *name)
{
	GLuint	vbo;
	GLuint	index;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	index = glGetAttribLocation(program_id, name);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	return (vbo);
}

GLuint			load_ebo(GLsizeiptr size, const GLvoid *data)
{
	GLuint    ebo;

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	return (ebo);
}

GLuint			load_col_vbo(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, const char *name)
{
	GLuint	vbo;
	GLuint	index;

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	index = glGetAttribLocation(program_id, name);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	return (vbo);
}

t_matrix		load_matrix(GLuint id, const char *name, float *m)
{
	t_matrix	matrix;

	matrix.id = glGetUniformLocation(id, name);
	matrix.m = m;
	glUniformMatrix4fv(matrix.id, 1, GL_FALSE, matrix.m);
	return (matrix);
}