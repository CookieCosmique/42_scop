/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 14:39:27 by bperreon          #+#    #+#             */
/*   Updated: 2016/01/12 15:15:27 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		fix_fps(void)
{
	static clock_t	t = 0;
	static time_t	t1 = 0;
	static size_t	frames = 0;

	if (clock() - t < CLOCKS_PER_SEC / FPS)
		usleep ((CLOCKS_PER_SEC / FPS - (clock() - t)) * 1000);
	t = clock();
	frames++;
	if (time(NULL) != t1)
	{
		printf("FPS: %zu\n", frames);
		frames = 0;
		t1 = time(NULL);
	}
}

void			render(t_env *e)
{

	glEnable(GL_DEPTH_TEST);

	glBindFramebuffer(GL_FRAMEBUFFER, e->sc_obj->msaa_buf);
	glClearColor(0.6f, 0.6f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindVertexArray(e->obj->vao);
	glUseProgram(e->line_program.id);
	//glDrawArrays(GL_TRIANGLES, 0, e->obj->vertices->size);
	glDrawElements(GL_TRIANGLES, e->obj->indices->size, GL_UNSIGNED_INT, 0);
	glClear(GL_DEPTH_BUFFER_BIT);
	glUseProgram(e->scene_program.id);
	//glDrawArrays(GL_TRIANGLES, 0, e->obj->vertices->size);
	glDrawElements(GL_TRIANGLES, e->obj->indices->size, GL_UNSIGNED_INT, 0);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, e->sc_obj->frame_buf);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, e->sc_obj->msaa_buf);
	glDrawBuffer(GL_BACK);
	glBlitFramebuffer(0, 0, WIDTH * XX_ALIAS , HEIGHT * XX_ALIAS, 
		              0, 0, WIDTH * XX_ALIAS , HEIGHT * XX_ALIAS, 
		              GL_COLOR_BUFFER_BIT, GL_NEAREST);
	
	glDisable(GL_DEPTH_TEST);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindVertexArray(e->sc_obj->vao);
	glUseProgram(e->screen_program.id);
	glDrawArrays(GL_TRIANGLES,0,6);

	glfwSwapBuffers(e->window);
	fix_fps();
}
