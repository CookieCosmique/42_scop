/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sc_object.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 12:45:44 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:17:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static t_sc_obj		*create_sc_points(void)
{
	t_sc_obj		*sc_obj;
	float			vertices[12] = {-1.0, -1.0, -1.0, 1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0, -1.0, 1.0, 1.0};
	float			tex_coord[12] = {0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0};

	if (!(sc_obj = (t_sc_obj*)ft_memalloc(sizeof(t_sc_obj))))
		return (NULL);
	sc_obj->vertices = (float*)ft_memalloc(sizeof(float) * 12);
	sc_obj->tex_coord = (float*)ft_memalloc(sizeof(float) * 12);
	if (!sc_obj->vertices || !sc_obj->tex_coord)
		return (NULL);
	memcpy(sc_obj->vertices, vertices, sizeof(float) * 12);
	memcpy(sc_obj->tex_coord, tex_coord, sizeof(float) * 12);
	return (sc_obj);
}

t_sc_obj			*load_sc_object(const GLuint program_id)
{
	t_sc_obj		*sc_obj;

	if (!(sc_obj = create_sc_points()))
		return (NULL);

	glGenVertexArrays(1, &(sc_obj->vao));
	glBindVertexArray(sc_obj->vao);

	sc_obj->vertices_vbo = load_2d_vbo(program_id, 12 * sizeof(float),
						sc_obj->vertices, "position");
	sc_obj->tex_vbo = load_2d_vbo(program_id, 12 * sizeof(float),
						sc_obj->tex_coord, "tex_coord");
	
	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1,&sc_obj->msaa_tex);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, sc_obj->msaa_tex);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, MSAA, GL_RGBA8, 
		                    WIDTH *XX_ALIAS, HEIGHT *XX_ALIAS, 0);

	glGenFramebuffers(1, &sc_obj->msaa_buf);
	glBindFramebuffer(GL_FRAMEBUFFER, sc_obj->msaa_buf);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
						   GL_TEXTURE_2D_MULTISAMPLE, sc_obj->msaa_tex, 0);

	glGenRenderbuffers(1,&sc_obj->rbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, sc_obj->rbo_depth);
	/*glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, 
						  WIDTH * XX_ALIAS, HEIGHT * XX_ALIAS);*/
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, MSAA,
			GL_DEPTH_COMPONENT32F, WIDTH * XX_ALIAS, HEIGHT * XX_ALIAS);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
		                      GL_RENDERBUFFER, sc_obj->rbo_depth);

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1,&sc_obj->frame_tex);
	glBindTexture(GL_TEXTURE_2D, sc_obj->frame_tex);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WIDTH *XX_ALIAS, HEIGHT *XX_ALIAS, 0,
			   	 GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	
	glGenFramebuffers(1, &sc_obj->frame_buf);
	glBindFramebuffer(GL_FRAMEBUFFER, sc_obj->frame_buf);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
						   GL_TEXTURE_2D, sc_obj->frame_tex, 0);

	glUseProgram(program_id);
	glUniform1f(glGetUniformLocation(program_id, "xx_alias"),XX_ALIAS);
	glUniform1f(glGetUniformLocation(program_id, "height"),HEIGHT);
	glUniform1f(glGetUniformLocation(program_id, "width"),WIDTH);

	check_error();

	return (sc_obj);
}
