/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/12 13:52:55 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/13 12:48:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

/*float		*create_grey_colors(const unsigned int size, const int coef)
{
	float			*colors;
	float			color;
	unsigned int	tmp;
	unsigned int	i;

	if (!(colors = (float*)malloc(sizeof(float) * size)))
		return (NULL);
	i = 0;
	while (i < size)
	{
		color = (float)((i / 9) % (size / coef)) / (size / coef - 1);
		tmp = i + 9;
		while (i < tmp && i < size)
		{
			colors[i] = color;
			i++;
		}
	}
	return (colors);
}*/

float 	*create_grey_colors(const unsigned int size)
{
	float 		*colors;
	unsigned int	i;
	if (!(colors = (float*)malloc(sizeof(float) * size)))
		return (NULL);
	i = 0;
	while (i < size)
	{
		colors[i] = i;
		i++;
	}
	return (colors);
}