/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 19:25:29 by bperreon          #+#    #+#             */
/*   Updated: 2016/01/12 15:15:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void	change_color(t_env *e)
{
	static int coef = 0;
	static int dir = 0;

	/*if (coef == 9)
		coef = e->obj->vertices->size / 20;
	else
		coef = 9;*/
	dir = !dir;
	coef ++;
	printf("coef : %d \n",coef);
	/*free(e->obj->colors);
	e->obj->colors = create_grey_colors(e->obj->vertices->size, coef);
	glBindBuffer(GL_ARRAY_BUFFER, e->obj->colors_vbo);
	glBufferData(GL_ARRAY_BUFFER, e->obj->vertices->size *
			e->obj->vertices->content_size, e->obj->colors, GL_STATIC_DRAW);*/
	glBindVertexArray(e->obj->vao);
	glUseProgram(e->scene_program.id);
	e->obj->uv = create_uv_datas(e->obj->uv, e->obj->vertices, dir);
	glBindBuffer(GL_ARRAY_BUFFER, e->obj->uv_vbo);
	glBufferData(GL_ARRAY_BUFFER, (e->obj->vertices->size * 2 / 3) *
			e->obj->vertices->content_size, e->obj->uv, GL_STATIC_DRAW);
	glUniform1i(e->scene_program.coef_id,coef);
}

static void	move_obj(t_env *e, float *move)
{
	static float	*trans = NULL;
	static float	*light = NULL;

	if (!trans)
		trans = e->scene_program.trans.m;
	if (!light)
		light = e->scene_program.light.m;
	
	matrix_identity(trans);
	matrix_rotate(trans, point3(0, 1, 0), move[0] / 10);
	matrix_rotate(trans, point3(1, 0, 0), move[1] / 10);
	matrix_translate(trans, point3(-e->obj->center[0], -e->obj->center[1],
				-e->obj->center[2]));
	matrix_scale(trans, point3(move[5] / 20, move[5] / 20, move[5] / 20));
	trans[12] += move[3] / 10;
	trans[13] += move[2] / 10;
	trans[14] += move[4] / 10;

	matrix_identity(light);
	matrix_rotate(light, point3(0, 1, 0), move[6] / 20);
	matrix_rotate(light, point3(1, 0, 0), move[7] / 20);

	glUseProgram(e->line_program.id);
	glUniformMatrix4fv(e->line_program.trans.id, 1, GL_FALSE, trans);
	glUseProgram(e->scene_program.id);
	glUniformMatrix4fv(e->scene_program.trans.id, 1, GL_FALSE, trans);
	glUniformMatrix4fv(e->scene_program.light.id, 1, GL_FALSE, light);
	glUniform1f(e->scene_program.pow_id,move[8]);
	glUniform1f(e->scene_program.diff_id,move[9]);
	glUniform1f(e->scene_program.face_id,move[10]);
}

static void	swap_textures(t_env *e)
{
	static int		t = 0;
	static float	ratio = 0;
	static float	target = 0;
	static int		c = 0;

	ratio += (target - ratio) * 0.05f;
	if (glfwGetKey(e->window, GLFW_KEY_T) == GLFW_PRESS && t == 0)
	{
		t = 1;
		target = !target;
	}
	if (glfwGetKey(e->window, GLFW_KEY_T) == GLFW_RELEASE)
		t = 0;
	glUseProgram(e->scene_program.id);
	glUniform1f(e->scene_program.ratio_id, ratio);
	c -= c * (glfwGetKey(e->window, GLFW_KEY_C) == GLFW_RELEASE);
	if (glfwGetKey(e->window, GLFW_KEY_C) == GLFW_PRESS && c == 0)
	{
		change_color(e);
		c = 1;
	}
}

static void	swap_shadow(t_env *e)
{
	static int		t = 0;
	static float	ratio = 0;
	static float	target = 0;

	ratio += (target - ratio) * 0.05f;
	if (glfwGetKey(e->window, GLFW_KEY_R) == GLFW_PRESS && t == 0)
	{
		t = 1;
		target = !target;
	}
	if (glfwGetKey(e->window, GLFW_KEY_R) == GLFW_RELEASE)
		t = 0;
	glUseProgram(e->scene_program.id);
	glUniform1f(e->scene_program.shadow_id, ratio);
}

static float swap_auto_rotate(t_env *e)
{
	static int		t = 0;
	static float	ratio = 1;
	static float	target = 1;

	ratio += (target - ratio) * 0.05f;
	if (glfwGetKey(e->window, GLFW_KEY_H) == GLFW_PRESS && t == 0)
	{
		t = 1;
		target = !target;
	}
	if (glfwGetKey(e->window, GLFW_KEY_H) == GLFW_RELEASE)
		t = 0;
	return ratio;
}

static void	swap_alpha(t_env *e)
{
	static int		t = 0;
	static float	ratio = 1;
	static float	target = 1;

	float speed = (target - ratio) * 0.2f;
	float max = 0.025f;
	
	if(speed > 0 && speed > max)
	{
		speed = max;
	}
	else if (speed < 0 && speed < -max)
	{
		speed = -max;
	}
	ratio += speed;
	if (glfwGetKey(e->window, GLFW_KEY_E) == GLFW_PRESS && t == 0)
	{
		t = 1;
		target = !target;
	}
	if (glfwGetKey(e->window, GLFW_KEY_E) == GLFW_RELEASE)
		t = 0;
	glUseProgram(e->line_program.id);
	glUniform1f(e->line_program.alpha_line_id, ratio);
}

static void	swap_normals(t_env *e)
{
	static int		t = 0;
	static float	ratio = 0;
	static float	target = 0;

	float speed = (target - ratio) * 0.2f;
	ratio += speed;
	if (glfwGetKey(e->window, GLFW_KEY_KP_5) == GLFW_PRESS && t == 0)
	{
		t = 1;
		target = !target;
	}
	if (glfwGetKey(e->window, GLFW_KEY_KP_5) == GLFW_RELEASE)
		t = 0;
	glUseProgram(e->scene_program.id);
	glUniform1f(e->scene_program.inverse_norm_id, ratio);
}

static void	events(t_env *e)
{
	static float	move[12] = {0, -5.0f, 0, 0, 0, 20, 0.2, 0.2, 1.7, 0.7, 0.0, 1.0};

	glfwPollEvents();
	move[11] = swap_auto_rotate(e);
	if (glfwGetKey(e->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(e->window, GL_TRUE);
	move[0] += (glfwGetKey(e->window, GLFW_KEY_D) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_A) == GLFW_PRESS) - (0.1f * move[11]);
	move[1] += (glfwGetKey(e->window, GLFW_KEY_W) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_S) == GLFW_PRESS);
	move[2] += (glfwGetKey(e->window, GLFW_KEY_UP) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_DOWN) == GLFW_PRESS);
	move[3] += (glfwGetKey(e->window, GLFW_KEY_RIGHT) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_LEFT) == GLFW_PRESS);
	move[4] += (glfwGetKey(e->window, GLFW_KEY_O) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_L) == GLFW_PRESS);
	move[5] += (glfwGetKey(e->window, GLFW_KEY_Z) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_X) == GLFW_PRESS);	
	move[6] += (glfwGetKey(e->window, GLFW_KEY_KP_4) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_KP_6) == GLFW_PRESS);
	move[7] += (glfwGetKey(e->window, GLFW_KEY_KP_8) == GLFW_PRESS) -
		(glfwGetKey(e->window, GLFW_KEY_KP_2) == GLFW_PRESS);
	if(glfwGetKey(e->window, GLFW_KEY_KP_9) == GLFW_PRESS)
		move[8] *= 1.1;
	if(glfwGetKey(e->window, GLFW_KEY_KP_7) == GLFW_PRESS)
		move[8] /= 1.1;
	move[9] += (glfwGetKey(e->window, GLFW_KEY_KP_3) == GLFW_PRESS)*0.05 -
		(glfwGetKey(e->window, GLFW_KEY_KP_1) == GLFW_PRESS)*0.05;
	move[10] += (glfwGetKey(e->window, GLFW_KEY_N) == GLFW_PRESS)*0.05 -
		(glfwGetKey(e->window, GLFW_KEY_B) == GLFW_PRESS)*0.05;
	if(move[8] < 0.000001)
		move[8] = 0.000001;
	if(move[8] > 100000)
		move[8] = 100000;
	if(move[9] < 0)
		move[9] = 0;
	if(move[9] > 1)
		move[9] = 1;
	if(move[10] < 0)
		move[10] = 0;
	if(move[10] > 1)
		move[10] = 1;
	move_obj(e, move);
	swap_textures(e);
	swap_shadow(e);
	swap_alpha(e);
	swap_normals(e);
}

void		loop(t_env *e)
{
	printf("size: %zu\n", e->obj->vertices->size);
	while (!glfwWindowShouldClose(e->window))
	{
		events(e);
		render(e);
	}
}
