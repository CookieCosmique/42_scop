/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_shader.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:32:28 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:39:35 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

void		shader_delete(GLuint program, GLuint shader)
{
	glDetachShader(program, shader);
	glDeleteShader(shader);
}

GLuint		load_shader(const char *filename, GLenum shader_type)
{
	GLuint			shader;
	GLchar const	*source;
	GLint			status;
	char			buffer[512];

	if ((source = read_file(filename)) == NULL)
		return (0);
	if ((shader = glCreateShader(shader_type)) == 0)
	{
		check_error();
		return (0);
	}
	glShaderSource(shader, 1, &source, NULL);
	free((char*)source);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (check_error() || status == GL_FALSE)
	{
		if(status == GL_FALSE)
		{
			glGetShaderInfoLog(shader, 512, NULL, buffer);
			printf("shader %s error : \n %s \n", filename, buffer);
		}
		return (0);
	}
	return (shader);
}
