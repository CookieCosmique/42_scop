/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 12:45:44 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:17:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static int		define_center(t_obj *obj)
{
	float		min[3];
	float		max[3];
	size_t		i;
	size_t		j;
	float		value;

	i = 0;
	while (i < obj->vertices->size)
	{
		if(obj->vert_faces[i/3] != NULL && 
			obj->vert_faces[i/3]->size > 0)
		{		
			j = -1;
			while (++j < 3)
			{
				value = ((float*)obj->vertices->data)[i + j];
				if (i == 0 || value < min[j])
					min[j] = value;
				if (i == 0 || value > max[j])
					max[j] = value;
			}
		}
		i += 3;
	}
	i = -1;
	while (++i < 3)
		obj->center[i] = (min[i] + max[i]) / 2;
	obj->size = 0;
	i = -1;
	while (++i < 3)
		obj->size += (max[i] - min[i]) / 3;
	return (1);
}

t_obj			*load_object(const char *filename, const GLuint program_id)
{
	t_obj		*obj;

	if (!(obj = parse_file(filename)))
		return (NULL);
	define_center(obj);
	glGenVertexArrays(1, &(obj->vao));
	glBindVertexArray(obj->vao);
	glUseProgram(program_id);
	obj->points_vbo = load_3d_vbo(program_id, obj->vertices->size *
			obj->vertices->content_size, obj->vertices->data, "position");
	//obj->colors = create_grey_colors(obj->vertices->size, 9);
	obj->colors = create_grey_colors(obj->vertices->size);
	obj->colors_vbo = load_col_vbo(program_id, obj->vertices->size *
			obj->vertices->content_size, obj->colors, "color");
	load_bmp_texture("textures/dickbutt.bmp", &(obj->texture));
	load_uv(obj, program_id);
	obj->normals_vbo = load_3d_vbo(program_id, obj->vertices->size *
			obj->vertices->content_size, obj->vert_norm, "normals");
	/*obj->lines_ebo = load_ebo(obj->lines_list->size *
		obj->lines_list->content_size, obj->lines_list->data);*/
	obj->indices_ebo = load_ebo(obj->indices->size * 
		obj->indices->content_size, obj->indices->data);
	printf("center : %f,%f,%f \n",
		obj->center[0],obj->center[1],obj->center[2]);
	printf("size : %f \n",obj->size);
	glUniform3fv(glGetUniformLocation(program_id, "center"), 1, obj->center);
	glUniform1f(glGetUniformLocation(program_id, "size"),obj->size);
	return (obj);
}
