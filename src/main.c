/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:10:22 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:40:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		delete_env(t_env *env)
{
	shader_delete(env->scene_program.id, env->scene_program.vertex_shader);
	shader_delete(env->scene_program.id, env->scene_program.fragment_shader);
}

static void		run(const char *filename)
{
	t_env		env;

	bzero(&env, sizeof(t_env));
	env.flags = 7;
	ft_log(LOG_FINE, "Environement initialization.");
	if (!init_env(&env, filename))
		return ;
	ft_log(LOG_FINE, "Environement initialized.");
	loop(&env);
	delete_env(&env);
	glfwTerminate();
}

int				main(int ac, char **av)
{
	if (ac == 2)
		run(av[1]);
	return (0);
}
