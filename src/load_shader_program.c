/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_shader_program.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:10:22 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:40:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"


t_shader_program		load_geo_shader_program(char *vertex_src,
	char *fragment_src, char *geometry_src)
{
	t_shader_program	sp;

	ft_log(LOG_FINE, "Loading geometry shader program.");
	if (!(sp.vertex_shader = load_shader(vertex_src, GL_VERTEX_SHADER)))
		ft_log(LOG_ERROR, "Shader error: %s \n",vertex_src);
	if (!(sp.fragment_shader = load_shader(fragment_src, GL_FRAGMENT_SHADER)))
		ft_log(LOG_ERROR, "Shader error: %s \n",fragment_src);
	if (!(sp.geometry_shader = load_shader(geometry_src, GL_GEOMETRY_SHADER)))
		ft_log(LOG_ERROR, "Shader error: %s \n",fragment_src);
	sp.id = glCreateProgram();
	glAttachShader(sp.id, sp.vertex_shader);
	glAttachShader(sp.id, sp.fragment_shader);
	glAttachShader(sp.id, sp.geometry_shader);
	glBindFragDataLocation(sp.id, 0, "outColor");
	glLinkProgram(sp.id);
	return (sp);
}

t_shader_program		load_shader_program(char *vertex_src,
	char *fragment_src)
{
	t_shader_program	sp;

	ft_log(LOG_FINE, "Loading shader program.");
	if (!(sp.vertex_shader = load_shader(vertex_src, GL_VERTEX_SHADER)))
		ft_log(LOG_ERROR, "Shader error: %s \n", vertex_src);
	if (!(sp.fragment_shader = load_shader(fragment_src, GL_FRAGMENT_SHADER)))
		ft_log(LOG_ERROR, "Shader error: %s \n", fragment_src);
	sp.id = glCreateProgram();
	glAttachShader(sp.id, sp.vertex_shader);
	glAttachShader(sp.id, sp.fragment_shader);
	glBindFragDataLocation(sp.id, 0, "outColor");
	glLinkProgram(sp.id);
	return (sp);
}

t_shader_program		load_shader_scene_program(void)
{
	t_shader_program	sp;

	ft_log(LOG_FINE, "Loading shader scene program.");
	sp = load_geo_shader_program("shaders/scene.vert", "shaders/scene.frag",
		"shaders/scene.geom");
	glUseProgram(sp.id);
	sp.trans = load_matrix(sp.id, "transformationMatrix",
		matrix_identity(new_matrix()));
	sp.light = load_matrix(sp.id, "lightMatrix",
		matrix_identity(new_matrix()));
	sp.proj = load_matrix(sp.id, "projectionMatrix",
		matrix_projection(70, 0.5, 1000, (float)WIDTH / (float)HEIGHT));
	sp.view = load_matrix(sp.id, "viewMatrix",
		matrix_view(new_matrix(), 0, 0, point3(0, 0, 10)));
	sp.ratio_id = glGetUniformLocation(sp.id, "ratio");
	sp.typ_id = glGetUniformLocation(sp.id, "typ");
	sp.shadow_id = glGetUniformLocation(sp.id, "shadow_ratio");
    sp.pow_id = glGetUniformLocation(sp.id, "power");
    sp.diff_id = glGetUniformLocation(sp.id, "diffuse");
    sp.face_id = glGetUniformLocation(sp.id, "per_faces");
    sp.inverse_norm_id = glGetUniformLocation(sp.id, "inverse_norm");
    sp.nbr_vertex_id = glGetUniformLocation(sp.id, "nbr_vertex");
    sp.coef_id = glGetUniformLocation(sp.id, "coef");
	glUniform1f(sp.ratio_id, 0.0f);
	glUniform1i(glGetUniformLocation(sp.id, "mytextureSampler"), 0);
	check_error();
	return (sp);
}

t_shader_program		load_shader_screen_program(void)
{
	t_shader_program	sp;

	ft_log(LOG_FINE, "Loading shader screen program.");
	sp = load_shader_program("shaders/screen.vert", "shaders/screen.frag");
	glUseProgram(sp.id);
	glUniform1i(glGetUniformLocation(sp.id, "tex_buf"), 1);
	check_error();
	return (sp);
}

t_shader_program		load_shader_line_program(void)
{
	t_shader_program	sp;

	ft_log(LOG_FINE, "Loading line shader program.");
	sp = load_geo_shader_program("shaders/line.vert", "shaders/line.frag",
		"shaders/line.geom");
	glUseProgram(sp.id);
	sp.trans = load_matrix(sp.id, "transformationMatrix",
			matrix_identity(new_matrix()));
	sp.proj = load_matrix(sp.id, "projectionMatrix",
			matrix_projection(70,0.5,1000, (float)WIDTH / (float)HEIGHT));
	sp.view = load_matrix(sp.id, "viewMatrix",
			matrix_view(new_matrix(), 0, 0, point3(0, 0, 10)));
	sp.alpha_line_id = glGetUniformLocation(sp.id, "alpha_line");
	check_error();
	return (sp);
}