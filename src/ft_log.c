/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 18:28:12 by bperreon          #+#    #+#             */
/*   Updated: 2015/05/31 19:36:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_log.h"
#include "color.h"

int		ft_log(t_logtype type, const char *str, ...)
{
	static const char	*typename[5] = {"ERROR", "FINE", "INFO", "WARNING", "DEBUG"};
	static const char	*colors[5] = {C_RED, C_GREEN, C_BWHITE, C_YELLOW, C_BLUE};
	va_list				arg;
	char				buffer[1024];

	va_start(arg, str);
	vsprintf(buffer, str, arg);
	va_end(arg);
	printf("%s[%s]%s\t%s\n", colors[type], typename[type], C_RESET, buffer);
	return (type);
}
