/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/20 19:21:35 by bperreon          #+#    #+#             */
/*   Updated: 2015/12/02 03:27:20 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

GLuint		load_bmp_texture(const char *imagepath, t_texture *texture)
{
	unsigned char	hd[54];
	unsigned int	data_pos;
	FILE			*file;

	if (!(file = fopen(imagepath, "rb")))
		return (0);
	if (fread(hd, 1, 54, file) != 54 || hd[0] != 'B' || hd[1] != 'M')
		return (0);
	if ((data_pos = *(int*)&(hd[0x0A])) == 0)
		data_pos = 54;
	texture->width = *(int*)&(hd[0x12]);
	texture->height = *(int*)&(hd[0x16]);
	if ((texture->image_size = *(int*)&(hd[0x22])) == 0)
		texture->image_size = texture->width * texture->height * 3;
	texture->data = (unsigned char*)ft_memalloc(sizeof(unsigned char) *
			texture->image_size);
	fread(texture->data, 1, texture->image_size, file);
	fclose(file);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &(texture->id));
	glBindTexture(GL_TEXTURE_2D, texture->id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width, texture->height,
			0, GL_BGR, GL_UNSIGNED_BYTE, texture->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return (texture->id);
}

float		*create_uv_datas(float *uv, t_list *vertices, int dir)
{
	size_t	i;

	if (dir != 0)
		dir = 1;
	i = 0;
	while (i < vertices->size)
	{
		uv[i / 3 * 2] = ((float*)vertices->data)[i + 2 - dir * 2] +
			((float*)vertices->data)[i + dir];
		uv[i / 3 * 2 + 1] = ((float*)vertices->data)[i + 1 + dir] +
			((float*)vertices->data)[i];
		i += 3;
	}
	return (uv);
}

int			load_uv(t_obj *obj, const GLuint program_id)
{
	GLint	uvattrib;

	if (!(obj->uv = (float*)ft_memalloc(sizeof(float) *
					(obj->vertices->size * 2 / 3))))
		return (0);
	create_uv_datas(obj->uv, obj->vertices, 0);
	glGenBuffers(1, &(obj->uv_vbo));
	glBindBuffer(GL_ARRAY_BUFFER, obj->uv_vbo);
	glBufferData(GL_ARRAY_BUFFER, (obj->vertices->size * 2 / 3) *
			obj->vertices->content_size, obj->uv, GL_STATIC_DRAW);
	uvattrib = glGetAttribLocation(program_id, "vertexUV");
	glEnableVertexAttribArray(uvattrib);
	glVertexAttribPointer(uvattrib, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	return (1);
}
