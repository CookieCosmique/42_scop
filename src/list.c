/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 16:08:23 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/20 18:26:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list			*list_new(size_t const content_size)
{
	t_list		*new;

	if (!(new = (t_list*)malloc(sizeof(t_list))))
		return (NULL);
	if (!(new->data = calloc(128, content_size)))
		return (NULL);
	new->size = 0;
	new->content_size = content_size;
	new->allocated = 128;
	return (new);
}

void			list_add(t_list *list, void const *data)
{
	if (!list)
		return ;
	if (list->size == list->allocated)
	{
		if (!(list->data = realloc(list->data,
						list->content_size * list->allocated * 2)))
			return ;
		list->allocated = list->allocated << 1;
	}
	memcpy(list->data + list->content_size * list->size, data,
			list->content_size);
	list->size++;
}

void			list_remove(t_list *list, size_t index)
{
	if (!list)
		return ;
	if (index >= list->size)
		return ;
}

void			list_clear(t_list *list)
{
	if (!list)
		return ;
	bzero(list->data, list->content_size * list->allocated);
	list->size = 0;
	list->allocated = 128;
	if (!(list->data = realloc(list->data, list->content_size * 128)))
		return ;
}

void			list_delete(t_list *list)
{
	if (!list)
		return ;
	bzero(list->data, list->content_size * list->allocated);
	free(list->data);
	bzero(list, sizeof(t_list));
	free(list);
}
