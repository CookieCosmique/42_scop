/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 18:52:56 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/12 12:02:53 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static					void	prepare_glew(void)
{
	# if defined(_WIN32) || defined(__CYGWIN__)
		ft_log(LOG_FINE, "Glew initialization");
		GLenum err = glewInit();
		if (err != GLEW_OK)
		{
			ft_log(LOG_ERROR, "Couldnt initialize glew: %s", glewGetErrorString(err));
			exit(EXIT_FAILURE);
		}
	# endif
}

static GLFWwindow		*init_window(const char *name)
{
	GLFWwindow		*window;

	ft_log(LOG_FINE, "Window initialization");
	if (!glfwInit())
		return (NULL);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
/*	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);*/
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	if (FULLSCREEN)
		window = glfwCreateWindow(WIDTH, HEIGHT, name, glfwGetPrimaryMonitor(), NULL);
	else
		window = glfwCreateWindow(WIDTH, HEIGHT, name, NULL, NULL);
	if (window == NULL)
		return (NULL);
	glfwMakeContextCurrent(window);
	return (window);
}

static int				init_screen_program(t_env *e)
{
	ft_log(LOG_FINE, "screen program initialization");
	e->screen_program = load_shader_screen_program();
	if (!(e->sc_obj = load_sc_object(e->screen_program.id)))
		return (0);
	return (1);
}

static int				init_line_program(t_env *e)
{
	ft_log(LOG_FINE, "line program initialization");
	e->line_program = load_shader_line_program();
	return (1);
}

int 					init_env(t_env *env, const char *filename)
{
	if (!(env->window = init_window(filename)))
		return (ft_log(LOG_ERROR, "Failed to create window."));
	prepare_glew();
	if (env->flags & F_SCREEN)
		init_screen_program(env);
	if (env->flags & F_LINES)
		init_line_program(env);
	env->scene_program = load_shader_scene_program();
	if (!(env->obj = load_object(filename, env->scene_program.id)))
		return (0);
	glUseProgram(env->scene_program.id);
	glUniform1i(env->scene_program.nbr_vertex_id, env->obj->vertices->size);
	glUniform1i(env->scene_program.coef_id, 9);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_CULL_FACE);
//	glEnable(GL_MULTISAMPLE);
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	glViewport(0, 0, WIDTH * XX_ALIAS , HEIGHT * XX_ALIAS);
//	glLineWidth(2 * XX_ALIAS);
	check_error();
	return (1);
}