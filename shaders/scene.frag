#version 400 core

in vec4 Color;
in vec2 UV;
in float Light_Angle;

out vec4 outColor;

uniform float ratio;
uniform float shadow_ratio;
uniform sampler2D myTextureSampler;

void main()
{    
    vec3 out_col;
    vec3 norm_col;
    float norm_val;

    norm_col = vec3(Color.r, Color.g, Color.b);
	out_col = mix(norm_col, texture(myTextureSampler, UV).rgb, ratio);
    norm_val = 0.9 * Light_Angle + 0.1;
    
    //outColor = vec4(out_col * (Color.a * (shadow_ratio) + (1-shadow_ratio)) * (norm_val*(1-shadow_ratio) + shadow_ratio) , 1);
    outColor = vec4(out_col * ( (0.8 + (0.2*shadow_ratio)) * Color.a +  (0.2 * (1 - shadow_ratio))) * (norm_val*(1-shadow_ratio) + shadow_ratio) , 1);
}