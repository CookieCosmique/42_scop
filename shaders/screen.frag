#version 400 core

in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D tex_buf;
uniform float xx_alias;

uniform float width;
uniform float height;

void main()
{
	int true_alias = int(xx_alias);		
	
	int i = 0;
	int j = 0;

	vec4 tmp_out = vec4(0,0,0,0);
	
	float new_x = Texcoord.x*true_alias;
	float new_y = Texcoord.y*true_alias;

	
	if(xx_alias > 1)
	{
		if(new_x <= width && new_y <= height)
		{
			float mov_1_x = 1.0/float(xx_alias*width);
			float mov_1_y = 1.0/float(xx_alias*height);
			
			while(i < true_alias)
			{
				j = 0;
				while(j < true_alias)
				{
					//tmp_out += texture(tex_buf, vec2(new_x - i*mov_1_x,new_y - j*mov_1_y));
					tmp_out += textureOffset(tex_buf, Texcoord*true_alias, ivec2(i,j));
					j++;
				}
				i++;
			}
			tmp_out = tmp_out/ float(true_alias*true_alias);
		}
		else
		{
			discard;
		}
	}
	else
	{
		tmp_out = texture(tex_buf,Texcoord);
	}

	outColor = tmp_out;
}