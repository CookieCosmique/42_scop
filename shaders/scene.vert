#version 400 core
in vec3 position;
in vec3 normals;
in float color;

in vec2 vertexUV;

out float vColor;
out float vAlpha;
out vec2 vUV;
out vec3 vPos;
out vec3 vNorm;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 center;
uniform float size;

void main()
{
	vec3 out_pos = vec3(position.x,position.y,position.z);

	mat4 tmp_trans = viewMatrix * transformationMatrix;

	vec4 tmp_to_proj = tmp_trans * vec4(out_pos, 1.0);
	vec4 centertrans = tmp_trans * vec4(center, 1.0);

	vec3 tmp_dist_1 = vec3((tmp_to_proj.x),(tmp_to_proj.y),(tmp_to_proj.z));
	vec3 tmp_dist_2 = vec3((centertrans.x),(centertrans.y),(centertrans.z));
	float	alpha = 1*(0.5 - (length(tmp_dist_1) - length(tmp_dist_2))/size);

	if (alpha > 1)
		alpha = 1;
	if (alpha < 0.1)
		alpha = 0.1;

	vColor = color;
	vAlpha = alpha;
	vUV = vertexUV;
	vPos = vec3(transformationMatrix * vec4(position,1.0));
	vNorm = vec3(transformationMatrix * vec4(normals,1.0) - transformationMatrix * vec4(0.0,0.0,0.0,1.0));
	gl_Position = projectionMatrix * tmp_to_proj;
}