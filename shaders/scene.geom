#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in float vColor[];
in float vAlpha[];
in vec2 vUV[];
in vec3 vPos[];
in vec3 vNorm[];

out vec4 Color;
out vec2 UV;
out float Light_Angle;

uniform mat4 lightMatrix;
uniform mat4 viewMatrix;

uniform float power;
uniform float diffuse;
uniform float inverse_norm;
uniform float per_faces;
uniform int nbr_vertex;
uniform int coef;

float borned(float var)
{
    if(var > 1)
        var = 1;
    if(var < 0)
        var = 0;
    return var;
}

float uniformize(float var)
{
    var = borned(var);
    if(power >= 0)
    {
        return pow(var,power);
    }
    else
    {
        return 0;
    }
}

float moded(float a , float b)
{
    return a - (b * int(a/b));
}

float yoloize(float a, float b)
{
    return moded( (b-1) * moded(a,b) , b);
}

float calculate_light(vec3 norm, vec3 light, vec3 pos, float diffuse_ratio)
{
    vec3 norm_plan_x;
    vec3 norm_plan_y;
    vec3 light_reflect;

    float diff_light;
    float spec_light;

    norm_plan_x = norm;
    norm_plan_y = light;

    light_reflect = - norm_plan_y + 2*norm_plan_x*(dot(norm_plan_x,norm_plan_y));

    diff_light = borned(- dot(light,norm));
    spec_light = uniformize(dot(light_reflect,pos));

    return (diff_light * diffuse_ratio) + (spec_light * (1-diffuse_ratio));
}

float calculate_mix_shadow(vec3 norm, vec3 face_norm, vec3 light, vec3 pos, float diffuse_ratio, float faces_ratio)
{
    if(faces_ratio <= 0.0001)
    {
        return calculate_light(norm, light, pos, diffuse_ratio);
    }
    else if( 1 - faces_ratio <= 0.0001)
    {
        return calculate_light(face_norm, light, pos, diffuse_ratio);
    }
    else
    {   
        return calculate_light(norm, light, pos, diffuse_ratio)*(1 - faces_ratio) + calculate_light(face_norm, light, pos, diffuse_ratio) *faces_ratio;
    }
}

void main()
{
    vec3 light_angle = vec3(lightMatrix * vec4(0.0,0.0,-1.0,1.0));

    vec3 vec_x;
    vec3 vec_y;
    vec3 normal;

    vec3 norm_0;
    vec3 norm_1;
    vec3 norm_2;

    vec3 pos_c_0;
    vec3 pos_c_1;
    vec3 pos_c_2;

    float nbr_v;
    float numero_face;
    float triangle_Color;

    if(!(per_faces <= 0.0001))
    {
        vec_x = vPos[1] - vPos[0];
        vec_y = vPos[2] - vPos[0];
        normal.x = vec_x.y * vec_y.z - vec_x.z * vec_y.y;
        normal.y = vec_x.z * vec_y.x - vec_x.x * vec_y.z;
        normal.z = vec_x.x * vec_y.y - vec_x.y * vec_y.x;
        normal = normalize(normal);
        normal *= (1 - 2*inverse_norm);
    }

    norm_0 = (1 - 2*inverse_norm) * normalize(vNorm[0]);
    norm_1 = (1 - 2*inverse_norm) * normalize(vNorm[1]);
    norm_2 = (1 - 2*inverse_norm) * normalize(vNorm[2]);

    pos_c_0 = vec3(normalize(viewMatrix * vec4(vPos[0],1)));
    pos_c_1 = vec3(normalize(viewMatrix * vec4(vPos[1],1)));
    pos_c_2 = vec3(normalize(viewMatrix * vec4(vPos[2],1)));

    nbr_v = nbr_vertex;
    //numero_face = moded(moded(moded(moded(vColor[0],(nbr_v/coef)) + moded(vColor[1],(nbr_v/coef)),(nbr_v/coef)) + moded(vColor[2],(nbr_v/coef)),(nbr_v/coef)) * coef, (nbr_v/coef));
    numero_face = moded(moded(moded(yoloize(vColor[0] + 1,nbr_v) * yoloize(vColor[1] + 1,nbr_v) + 1,nbr_v) * yoloize(vColor[2] + 1,nbr_v) + 1,nbr_v) * coef + 1, nbr_v);

    //triangle_Color = numero_face/(nbr_v/coef);
    triangle_Color = numero_face/(nbr_v);

    gl_Position = gl_in[0].gl_Position;
    Color = vec4(triangle_Color,triangle_Color,triangle_Color,vAlpha[0]);
    UV = vUV[0];
    Light_Angle = calculate_mix_shadow(norm_0, normal, light_angle, pos_c_0, diffuse, per_faces);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    Color = vec4(triangle_Color,triangle_Color,triangle_Color,vAlpha[1]);
    UV = vUV[1];
    Light_Angle = calculate_mix_shadow(norm_1, normal, light_angle, pos_c_1, diffuse, per_faces);
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    Color = vec4(triangle_Color,triangle_Color,triangle_Color,vAlpha[2]);
    UV = vUV[2];
    Light_Angle = calculate_mix_shadow(norm_2, normal, light_angle, pos_c_2, diffuse, per_faces);
    EmitVertex();

    EndPrimitive();

}