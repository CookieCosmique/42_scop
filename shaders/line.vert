#version 400 core
in vec3 position;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main()
{
	vec3 out_pos = vec3(position.x,position.y,position.z);
	mat4 tmp_trans = viewMatrix * transformationMatrix;
	vec4 tmp_to_proj = tmp_trans * vec4(out_pos, 1.0);
	
	gl_Position = projectionMatrix * tmp_to_proj;
}