#version 400 core

out vec4 outColor;

uniform float alpha_line;

void main()
{    

    if(alpha_line < 0.01)
    {
        discard;
    }
    else
    {
        outColor = vec4(1.0,0.0,0.0,alpha_line);    
    }
}