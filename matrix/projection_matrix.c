/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projection_matrix.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/19 14:28:03 by bperreon          #+#    #+#             */
/*   Updated: 2015/10/19 15:26:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrix.h"

float		*matrix_projection(float fov, float near, float far, float aspect)
{
	float	*matrix;
	float	x_scale;
	float	y_scale;
	float	frustrum;

	if (!(matrix = new_matrix()))
		return (NULL);
	y_scale = (float)((1.0f / tan(TO_RADIAN(fov / 2.0f))) * aspect);
	x_scale = y_scale / aspect;
	frustrum = far - near;
	matrix[0] = x_scale;
	matrix[5] = y_scale;
	matrix[10] = -((far + near) / frustrum);
	matrix[11] = -((2 * near * far) / frustrum);
	matrix[14] = -1;
	matrix[15] = 0;
	return (matrix);
}

float		*matrix_view(float *matrix, float pitch, float yaw, t_point3 pos)
{
	matrix_identity(matrix);
	matrix_rotate(matrix, point3(1, 0, 0), pitch);
	matrix_rotate(matrix, point3(0, 1, 0), yaw);
	matrix_translate(matrix, point3(-pos.x, -pos.y, -pos.z));
	return (matrix);
}
