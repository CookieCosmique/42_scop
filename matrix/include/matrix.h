/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/16 10:12:44 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/20 19:00:52 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_H
# define MATRIX_H

# include <stdlib.h>
# include <math.h>

# define TO_DEGREE(x) ((int)(180 * x / M_PI) % 360)
# define TO_RADIAN(x) (x / 180.0f * M_PI)

typedef struct	s_point3
{
	float	x;
	float	y;
	float	z;
}				t_point3;

t_point3		point3(float x, float y, float z);
t_point3        cross_product(t_point3 a, t_point3 b);
t_point3        vec_from(t_point3 a, t_point3 b);
float			vec_norm(t_point3 vec);
t_point3		vec_normalize(t_point3 vec);

float			*new_matrix(void);
float			*matrix_rotate(float *m, t_point3 axis, float angle);
float			*matrix_scale(float *m, t_point3 vec);
float			*matrix_translate(float *m, t_point3 vec);
float			*matrix_identity(float *m);
float			*matrix_view(float *m, float pitch, float yaw, t_point3 pos);
float			*matrix_projection(float fov, float n, float f, float aspect);

float			*matrix_rotation_x(float *m, float angle);
float			*matrix_rotation_y(float *m, float angle);

#endif
