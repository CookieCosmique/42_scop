/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_rotation.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/20 13:46:18 by bperreon          #+#    #+#             */
/*   Updated: 2015/11/23 14:33:32 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "matrix.h"

float	*matrix_rotation_x(float *m, float angle)
{
	float	tmp[16];
	float	c;
	float	s;

	c = cos(angle);
	s = sin(angle);
	tmp[1] = m[1] * c + m[2] * s;
	tmp[5] = m[5] * c + m[6] * s;
	tmp[9] = m[9] * c + m[10] * s;
	tmp[13] = m[13] * c + m[14] * s;
	tmp[2] = m[1] * -s + m[2] * c;
	tmp[6] = m[5] * -s + m[6] * c;
	tmp[10] = m[9] * -s + m[10] * c;
	tmp[14] = m[13] * -s + m[14] * c;
	m[1] = tmp[1];
	m[5] = tmp[5];
	m[9] = tmp[9];
	m[13] = tmp[13];
	m[2] = tmp[2];
	m[6] = tmp[6];
	m[10] = tmp[10];
	m[14] = tmp[14];
	return (m);
}

float	*matrix_rotation_y(float *m, float angle)
{
	float	tmp[16];
	float	c;
	float	s;

	c = cos(angle);
	s = sin(angle);
	tmp[0] = c * m[0] + s * m[8];
	tmp[1] = c * m[1] + s * m[9];
	tmp[2] = c * m[2] + s * m[10];
	tmp[3] = c * m[3] + s * m[11];
	tmp[8] = -s * m[0] + c * m[8];
	tmp[9] = -s * m[1] + c * m[9];
	tmp[10] = -s * m[2] + c * m[10];
	tmp[11] = -s * m[3] + c * m[11];
	m[0] = tmp[0];
	m[1] = tmp[1];
	m[2] = tmp[2];
	m[3] = tmp[3];
	m[8] = tmp[8];
	m[9] = tmp[9];
	m[10] = tmp[10];
	m[11] = tmp[11];
	return (m);
}
