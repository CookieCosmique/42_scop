# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/22 17:51:18 by bperreon          #+#    #+#              #
#    Updated: 2016/01/16 15:34:55 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = scop
CC = gcc
CFLAGS = -Wall -Wextra -Werror -g3
LIB = -L./lib/ -lglfw3 -lgdi32 -lglew32 -lopengl32 ./matrix/matrix.a
LIB_FOLDER = ./lib/
HEADERS = -I./glfw/include -I./glew/include/GL/ -I./include -I./matrix/include
SOURCE = src/main.c src/read_file.c src/check_error.c src/load_shader.c \
		 src/ft_log.c src/init.c src/loop.c src/parsing.c src/colors.c \
		 src/list.c src/object.c src/texture.c src/render.c src/sc_object.c \
		 src/loaders.c src/ft_memalloc.c src/load_shader_program.c
OBJ = $(SOURCE:.c=.o)
GLFW = ./glfw/src/libglfw3.a
GLEW = ./glew/build/cmake/lib/libglew32.a

all: $(LIB_FOLDER) $(GLFW) $(GLEW) $(NAME)

$(NAME): $(OBJ)
	make -C matrix/
	$(CC) -o $(NAME) $(OBJ) $(LIB)

$(LIB_FOLDER):
	mkdir $@ -p

$(GLFW):
	git submodule init glfw
	git submodule update glfw
	cd glfw && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ..
	cp $(GLFW) $(LIB_FOLDER)

$(GLEW):
	git submodule init glew
	git submodule update glew
	make -C glew/auto
	mv glew/src/glew.c glew/src/glew_old.c
	echo "#define _WIN32" > glew/src/glew.c
	cat glew/src/glew_old.c >> glew/src/glew.c
	rm glew/src/glew_old.c
	cd glew/build/cmake && cmake . -DCMAKE_LEGACY_CYGWIN_WIN32=1 && make -j 8 -i && cd ../../..
	cp $(GLEW) $(LIB_FOLDER)

%.o: %.c include/*.h
	$(CC) $(CFLAGS) $(HEADERS) -c $< -o $@

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
